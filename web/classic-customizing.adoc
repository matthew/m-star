= Customizing your M-Star-Classic-equipped keyboard  =

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

Out of the box, a keyboard equipped with a M-Star Classic controller will
behave exactly like a stock Model M.

You need read no further unless you want to use the capabilities of
the QMK firmmare running on your M-Star Classic to change your keymap or
set up macros.

== Setting up QMK

You'll need to start by reading https://docs.qmk.fm/#/newbs[The QMK
Tutorial]. Unfortunately, the QMK online GUI does not yet support
the M-Star Classic; you will have to build your firmware by hand.

// FIXME: Check paths and "m-star-classic" before shipping
To finish your preparation, copy or symlink the contents of the
directory QMK/m-star-classic in the M-Star repository into
keyboards/m-star-classic in the QMK repository.  A typical command for a
default setup, if MYPATH is set to where you keep yor M-Star Classic code,
would be:

----------------------------------------------------------
$ ln -sf $MYPATH/QMK/m_star_classic ~/qmk_firmware/keyboards/m_star_classic
----------------------------------------------------------

This lets you use QMK to build and flash your keyboard firmware.

== Reflashing your keyboard firmware

Warning: if you have two M-Star-Classic-equipped keyboards plugged in when
the flash command runs, QMK can get confused about which one to ship
to! So don't do that.

Our keyboard type is "m-star-classic".  To find out what keymaps are defined, see
https://gitlab.com/esr/m-star/-/tree/main/QMK/m-star-classic/README.adoc[this
list].

This example builds and flashes the default keymap, but you can give
the name of any variant you like as a last argument.

-----------------------------------------
$ qmk flash -kb m_star_classic -km default
-----------------------------------------

You should see output similar to:

-------------------------------------------------------
Compiling keymap with gmake -j 1 m-star-classic:default:flash

QMK Firmware 0.13.28
Making m-star-classic with keymap default and target flash

arm-none-eabi-gcc (15:9-2019-q4-0ubuntu2) 9.2.1 20191025 (release) [ARM/arm-9-branch revision 277599]
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Size before:
   text    data     bss     dec     hex filename
      0   35762       0   35762    8bb2 .build/m-star-classic_default.hex

Compiling: quantum/command.c                                                                        [OK]
Compiling: quantum/process_keycode/process_terminal.c                                               [OK]
Linking: .build/m-star-classic_wendell.elf                                                          [OK]
Creating binary load file for flashing: .build/m-star-classic_wendell.bin                           [OK]
Creating load file for flashing: .build/m-star-classic_default.hex                                  [OK]

Size after:
   text    data     bss     dec     hex filename
      0   35762       0   35762    8bb2 .build/m-star-classic_default.hex

Copying m-star-classic_default.bin to qmk_firmware folder                                           [OK]
-------------------------------------------------------

When you see this message...

-------------------------------------------------------
Bootloader not found. Trying again every 0.5s (Ctrl+C to cancel)...........
-------------------------------------------------------

...simply unplug and replug your keyboard. On bootup the keyboard
waits for 250msec to download a new firmware image before resuming the boot
operation (for now, as a failsafe).

//end

