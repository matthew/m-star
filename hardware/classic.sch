EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "M-Star Classic"
Date "2021-07-29"
Rev "v0.02"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5E891730
P 8300 4750
F 0 "J4" H 8380 4742 50  0000 L CNN
F 1 "LEDs" H 8380 4651 50  0000 L CNN
F 2 "common:triomate-leds4" H 8300 4750 50  0001 C CNN
F 3 "~" H 8300 4750 50  0001 C CNN
	1    8300 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 1200 6650 1200
Wire Wire Line
	6650 1300 6950 1300
Wire Wire Line
	6950 1400 6650 1400
Wire Wire Line
	6650 1500 6950 1500
Wire Wire Line
	6950 1600 6650 1600
Wire Wire Line
	6650 1700 6950 1700
Wire Wire Line
	6950 1800 6650 1800
Wire Wire Line
	6950 2000 6650 2000
Wire Wire Line
	6650 2100 6950 2100
Wire Wire Line
	6950 2200 6650 2200
Wire Wire Line
	6650 2300 6950 2300
Wire Wire Line
	6950 2400 6650 2400
Wire Wire Line
	6650 2500 6950 2500
Wire Wire Line
	6950 2600 6650 2600
Wire Wire Line
	6650 2700 6950 2700
$Comp
L Device:R R8
U 1 1 5E920DCC
P 6900 4750
F 0 "R8" V 6850 4900 50  0000 C CNN
F 1 "100" V 6900 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 4750 50  0001 C CNN
F 3 "~" H 6900 4750 50  0001 C CNN
F 4 "C17408" H 6900 4750 50  0001 C CNN "LCSC"
	1    6900 4750
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5E923217
P 6900 4950
F 0 "R9" V 6850 5100 50  0000 C CNN
F 1 "100" V 6900 4950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 4950 50  0001 C CNN
F 3 "~" H 6900 4950 50  0001 C CNN
F 4 "C17408" H 6900 4950 50  0001 C CNN "LCSC"
	1    6900 4950
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 4650 6750 4650
Wire Wire Line
	6750 4750 6450 4750
Wire Wire Line
	6450 4950 6750 4950
$Comp
L Connector:USB_B J1
U 1 1 5EDA01F7
P 850 1300
F 0 "J1" H 907 1767 50  0000 C CNN
F 1 "USB_B" H 907 1676 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 1000 1250 50  0001 C CNN
F 3 " ~" H 1000 1250 50  0001 C CNN
	1    850  1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5EDA1464
P 1400 1950
F 0 "#PWR0103" H 1400 1700 50  0001 C CNN
F 1 "GND" H 1405 1777 50  0000 C CNN
F 2 "" H 1400 1950 50  0001 C CNN
F 3 "" H 1400 1950 50  0001 C CNN
	1    1400 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 1950 1400 1800
$Comp
L power:+5V #PWR0104
U 1 1 5EDAFF6A
P 2150 900
F 0 "#PWR0104" H 2150 750 50  0001 C CNN
F 1 "+5V" H 2165 1073 50  0000 C CNN
F 2 "" H 2150 900 50  0001 C CNN
F 3 "" H 2150 900 50  0001 C CNN
	1    2150 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5EDACAA8
P 1950 1300
F 0 "R3" V 2050 1300 50  0000 L CNN
F 1 "22" V 2050 1150 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1880 1300 50  0001 C CNN
F 3 "~" H 1950 1300 50  0001 C CNN
F 4 "C23345" H 1950 1300 50  0001 C CNN "LCSC"
	1    1950 1300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R4
U 1 1 5EDC1F8E
P 1950 1400
F 0 "R4" V 1850 1400 50  0000 L CNN
F 1 "22" V 1850 1250 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1880 1400 50  0001 C CNN
F 3 "~" H 1950 1400 50  0001 C CNN
F 4 "C23345" H 1950 1400 50  0001 C CNN "LCSC"
	1    1950 1400
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5F0D960E
P 1650 4850
F 0 "#PWR0121" H 1650 4600 50  0001 C CNN
F 1 "GND" H 1655 4677 50  0000 C CNN
F 2 "Connector:Banana_Jack_1Pin" H 1650 4850 50  0001 C CNN
F 3 "" H 1650 4850 50  0001 C CNN
	1    1650 4850
	0    1    1    0   
$EndComp
$Comp
L Device:C C10
U 1 1 5F169982
P 3800 1300
F 0 "C10" H 3915 1346 50  0000 L CNN
F 1 "100nF" H 3915 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3838 1150 50  0001 C CNN
F 3 "~" H 3800 1300 50  0001 C CNN
F 4 "C1525" H 3800 1300 50  0001 C CNN "LCSC"
	1    3800 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 5F1732B8
P 4300 1300
F 0 "C11" H 4415 1346 50  0000 L CNN
F 1 "100nF" H 4415 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4338 1150 50  0001 C CNN
F 3 "~" H 4300 1300 50  0001 C CNN
F 4 "C1525" H 4300 1300 50  0001 C CNN "LCSC"
	1    4300 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5F17C58D
P 4800 1300
F 0 "C12" H 4915 1346 50  0000 L CNN
F 1 "100nF" H 4915 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4838 1150 50  0001 C CNN
F 3 "~" H 4800 1300 50  0001 C CNN
F 4 "C1525" H 4800 1300 50  0001 C CNN "LCSC"
	1    4800 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5F1858EC
P 5300 1300
F 0 "C13" H 5415 1346 50  0000 L CNN
F 1 "100nF" H 5415 1255 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5338 1150 50  0001 C CNN
F 3 "~" H 5300 1300 50  0001 C CNN
F 4 "C1525" H 5300 1300 50  0001 C CNN "LCSC"
	1    5300 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1450 5800 1550
Wire Wire Line
	5800 1550 5300 1550
Wire Wire Line
	3800 1550 3800 1450
Wire Wire Line
	4300 1450 4300 1550
Connection ~ 4300 1550
Wire Wire Line
	4300 1550 3800 1550
Wire Wire Line
	4800 1450 4800 1550
Connection ~ 4800 1550
Wire Wire Line
	4800 1550 4300 1550
Wire Wire Line
	5300 1450 5300 1550
Connection ~ 5300 1550
Wire Wire Line
	5300 1550 4800 1550
Wire Wire Line
	5800 1150 5800 1050
Wire Wire Line
	5800 1050 5300 1050
Wire Wire Line
	3800 1050 3800 1150
Wire Wire Line
	4300 1050 4300 1150
Connection ~ 4300 1050
Wire Wire Line
	4300 1050 3800 1050
Wire Wire Line
	4800 1150 4800 1050
Connection ~ 4800 1050
Wire Wire Line
	4800 1050 4300 1050
Wire Wire Line
	5300 1150 5300 1050
Connection ~ 5300 1050
Wire Wire Line
	5300 1050 4800 1050
$Comp
L power:+3.3V #PWR0122
U 1 1 5F1EB308
P 3800 1050
F 0 "#PWR0122" H 3800 900 50  0001 C CNN
F 1 "+3.3V" H 3815 1223 50  0000 C CNN
F 2 "" H 3800 1050 50  0001 C CNN
F 3 "" H 3800 1050 50  0001 C CNN
	1    3800 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 5F1F6376
P 3800 1550
F 0 "#PWR0123" H 3800 1300 50  0001 C CNN
F 1 "GND" H 3805 1377 50  0000 C CNN
F 2 "" H 3800 1550 50  0001 C CNN
F 3 "" H 3800 1550 50  0001 C CNN
	1    3800 1550
	1    0    0    -1  
$EndComp
Text GLabel 6650 1200 0    50   Input ~ 0
col0
Text GLabel 6650 1300 0    50   Input ~ 0
col1
Text GLabel 6650 1400 0    50   Input ~ 0
col2
Text GLabel 6650 1500 0    50   Input ~ 0
col3
Text GLabel 6650 1600 0    50   Input ~ 0
col4
Text GLabel 6650 1700 0    50   Input ~ 0
col5
Text GLabel 6650 1800 0    50   Input ~ 0
col6
Text GLabel 6650 1900 0    50   Input ~ 0
col7
Text GLabel 6650 2000 0    50   Input ~ 0
col8
Text GLabel 6650 2100 0    50   Input ~ 0
col9
Text GLabel 6650 2200 0    50   Input ~ 0
col10
Text GLabel 6650 2300 0    50   Input ~ 0
col11
Text GLabel 6650 2400 0    50   Input ~ 0
col12
Text GLabel 6650 2500 0    50   Input ~ 0
col13
Text GLabel 6650 2600 0    50   Input ~ 0
col14
Text GLabel 6650 2700 0    50   Input ~ 0
col15
Text GLabel 6750 3050 0    50   Input ~ 0
row0
Text GLabel 6750 3150 0    50   Input ~ 0
row1
Text GLabel 6750 3250 0    50   Input ~ 0
row2
Text GLabel 6750 3350 0    50   Input ~ 0
row3
Text GLabel 6750 3450 0    50   Input ~ 0
row4
Text GLabel 6750 3550 0    50   Input ~ 0
row5
Text GLabel 6750 3650 0    50   Input ~ 0
row6
Text GLabel 6750 3750 0    50   Input ~ 0
row7
Text GLabel 2550 1300 2    50   Input ~ 0
D+
Text GLabel 2550 1400 2    50   Input ~ 0
D-
Wire Wire Line
	6850 3950 7050 3950
Wire Wire Line
	2400 1300 2400 1250
Wire Wire Line
	2100 1100 2150 1100
Wire Wire Line
	2150 1100 2150 900 
Wire Wire Line
	2400 1300 2100 1300
Wire Wire Line
	2550 1300 2400 1300
Connection ~ 2400 1300
Wire Wire Line
	2550 1400 2100 1400
$Comp
L power:+3.3V #PWR0119
U 1 1 5F15264C
P 1650 4700
F 0 "#PWR0119" H 1650 4550 50  0001 C CNN
F 1 "+3.3V" H 1665 4873 50  0000 C CNN
F 2 "" H 1650 4700 50  0001 C CNN
F 3 "" H 1650 4700 50  0001 C CNN
	1    1650 4700
	1    0    0    -1  
$EndComp
Text GLabel 2050 4450 0    50   Input ~ 0
led0
Text GLabel 2050 4550 0    50   Input ~ 0
led1
Text GLabel 2050 4650 0    50   Input ~ 0
led2
Wire Wire Line
	1650 4750 1650 4700
$Comp
L power:GND #PWR0125
U 1 1 5F1B8868
P 1650 4950
F 0 "#PWR0125" H 1650 4700 50  0001 C CNN
F 1 "GND" H 1655 4777 50  0000 C CNN
F 2 "" H 1650 4950 50  0001 C CNN
F 3 "" H 1650 4950 50  0001 C CNN
	1    1650 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4850 1650 4950
$Comp
L Connector:TestPoint TP1
U 1 1 5F23CFEB
P 700 4500
F 0 "TP1" H 642 4526 50  0000 R CNN
F 1 "TestPoint" H 642 4617 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 900 4500 50  0001 C CNN
F 3 "~" H 900 4500 50  0001 C CNN
	1    700  4500
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0126
U 1 1 5F25ACB8
P 700 4500
F 0 "#PWR0126" H 700 4350 50  0001 C CNN
F 1 "+5V" H 715 4673 50  0000 C CNN
F 2 "" H 700 4500 50  0001 C CNN
F 3 "" H 700 4500 50  0001 C CNN
	1    700  4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4450 2050 4450
Wire Wire Line
	2150 4550 2050 4550
Wire Wire Line
	2050 4650 2150 4650
$Comp
L Connector_Generic:Conn_01x05 J6
U 1 1 5F2B29EC
P 2350 4650
F 0 "J6" H 2430 4692 50  0000 L CNN
F 1 "LED Pin Hdr" H 2430 4601 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 2350 4650 50  0001 C CNN
F 3 "~" H 2350 4650 50  0001 C CNN
	1    2350 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4750 2150 4750
Wire Wire Line
	1650 4850 2150 4850
$Comp
L Device:R R16
U 1 1 5F9EF12F
P 1950 1100
F 0 "R16" V 1743 1100 50  0000 C CNN
F 1 "0" V 1834 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.30x2.65mm_HandSolder" V 1880 1100 50  0001 C CNN
F 3 "~" H 1950 1100 50  0001 C CNN
F 4 "C17477" H 1950 1100 50  0001 C CNN "LCSC"
	1    1950 1100
	0    1    1    0   
$EndComp
Text GLabel 5450 3850 2    50   Input ~ 0
row5
Text GLabel 5450 3750 2    50   Input ~ 0
row7
Text GLabel 5450 3650 2    50   Input ~ 0
row6
Text GLabel 5450 3550 2    50   Input ~ 0
col15
Text GLabel 5450 3450 2    50   Input ~ 0
col14
Text GLabel 5450 3350 2    50   Input ~ 0
col13
Text GLabel 5450 3250 2    50   Input ~ 0
col12
Text GLabel 5450 3150 2    50   Input ~ 0
col11
Text GLabel 3700 2850 0    50   Input ~ 0
col2
Text GLabel 3700 2750 0    50   Input ~ 0
col1
Text GLabel 3700 2650 0    50   Input ~ 0
col0
Wire Wire Line
	3800 3050 3750 3050
Wire Wire Line
	3800 3150 3750 3150
Wire Wire Line
	3800 3250 3750 3250
Wire Wire Line
	5400 3150 5450 3150
Wire Wire Line
	5400 3250 5450 3250
Wire Wire Line
	5400 3350 5450 3350
Wire Wire Line
	5400 3450 5450 3450
Wire Wire Line
	5400 3550 5450 3550
Wire Wire Line
	5400 3650 5450 3650
Wire Wire Line
	5400 3750 5450 3750
Wire Wire Line
	5400 3850 5450 3850
Text GLabel 3700 3350 0    50   Input ~ 0
D-
Text GLabel 3700 3450 0    50   Input ~ 0
D+
Text GLabel 3750 3550 0    50   Input ~ 0
row3
Text GLabel 3750 3650 0    50   Input ~ 0
row4
Text GLabel 3750 3750 0    50   Input ~ 0
row2
Text GLabel 3750 3850 0    50   Input ~ 0
row1
Text GLabel 3750 3950 0    50   Input ~ 0
row0
Text GLabel 3750 4050 0    50   Input ~ 0
led2
Text GLabel 3750 4150 0    50   Input ~ 0
led1
Text GLabel 3750 4250 0    50   Input ~ 0
led0
Text GLabel 3750 3250 0    50   Input ~ 0
col6
Text GLabel 3750 3150 0    50   Input ~ 0
col5
Text GLabel 3750 3050 0    50   Input ~ 0
col4
Text GLabel 3700 2950 0    50   Input ~ 0
col3
Text GLabel 5550 2850 2    50   Input ~ 0
col8
Text GLabel 5550 2950 2    50   Input ~ 0
col9
Text GLabel 5550 3050 2    50   Input ~ 0
col10
$Comp
L power:GND #PWR0127
U 1 1 610662FA
P 4250 4850
F 0 "#PWR0127" H 4250 4600 50  0001 C CNN
F 1 "GND" H 4255 4677 50  0000 C CNN
F 2 "" H 4250 4850 50  0001 C CNN
F 3 "" H 4250 4850 50  0001 C CNN
	1    4250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4850 4250 4650
Wire Wire Line
	5400 2950 5550 2950
Wire Wire Line
	5400 3050 5550 3050
Wire Wire Line
	3800 3350 3700 3350
Wire Wire Line
	3800 2950 3700 2950
Wire Wire Line
	3800 2850 3700 2850
Wire Wire Line
	3800 2750 3700 2750
Wire Wire Line
	3800 2650 3700 2650
Wire Wire Line
	3800 3450 3700 3450
Wire Wire Line
	3800 3550 3750 3550
Wire Wire Line
	3800 3650 3750 3650
Wire Wire Line
	3800 3750 3750 3750
Wire Wire Line
	3800 3850 3750 3850
Wire Wire Line
	3800 3950 3750 3950
Wire Wire Line
	3800 4050 3750 4050
Wire Wire Line
	3800 4150 3750 4150
Wire Wire Line
	3800 4250 3750 4250
$Comp
L power:+5V #PWR0128
U 1 1 611F0AFA
P 3900 2300
F 0 "#PWR0128" H 3900 2150 50  0001 C CNN
F 1 "+5V" H 3915 2473 50  0000 C CNN
F 2 "" H 3900 2300 50  0001 C CNN
F 3 "" H 3900 2300 50  0001 C CNN
	1    3900 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2300 4500 2300
Wire Wire Line
	4500 2300 4500 2350
Text GLabel 5550 2750 2    50   Input ~ 0
col7
Wire Wire Line
	5400 2850 5550 2850
$Comp
L YAAJ_BluePill_SWD_Breakout:YAAJ_BluePill_SWD_Breakout U3
U 1 1 60F5B570
P 4600 3450
F 0 "U3" H 4600 4731 50  0000 C CNN
F 1 "YAAJ_BluePill_SWD_Breakout" H 4600 4640 50  0000 C CNN
F 2 "STM32:YAAJ_BluePill_SWD_1" V 4525 4400 50  0001 C CNN
F 3 "" V 4525 4400 50  0001 C CNN
	1    4600 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5F18ED8D
P 5800 1300
F 0 "C14" H 5915 1346 50  0000 L CNN
F 1 "100nF" H 5915 1255 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_5x5.4" H 5838 1150 50  0001 C CNN
F 3 "~" H 5800 1300 50  0001 C CNN
F 4 "C1525" H 5800 1300 50  0001 C CNN "LCSC"
	1    5800 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E91FFA8
P 6900 4650
F 0 "R7" V 6850 4800 50  0000 C CNN
F 1 "100" V 6900 4650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 4650 50  0001 C CNN
F 3 "~" H 6900 4650 50  0001 C CNN
F 4 "C17408" H 6900 4650 50  0001 C CNN "LCSC"
	1    6900 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2750 5550 2750
Wire Wire Line
	6650 1900 6950 1900
$Comp
L Connector:USB_B J1b1
U 1 1 613A393F
P 1050 2650
F 0 "J1b1" H 1107 3117 50  0000 C CNN
F 1 "USB_B-Alternate" H 1107 3026 50  0000 C CNN
F 2 "Connector_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 1200 2600 50  0001 C CNN
F 3 " ~" H 1200 2600 50  0001 C CNN
	1    1050 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	850  1700 750  1700
Wire Wire Line
	850  1700 850  1800
Connection ~ 850  1700
Text GLabel 5450 3950 2    50   Input ~ 0
row8
Text GLabel 5450 4050 2    50   Input ~ 0
row9
Text GLabel 5450 4150 2    50   Input ~ 0
row10
Wire Wire Line
	5400 3950 5450 3950
Wire Wire Line
	5400 4050 5450 4050
Wire Wire Line
	5400 4150 5450 4150
Text GLabel 6900 800  0    50   Input ~ 0
row8
Text GLabel 6900 900  0    50   Input ~ 0
row9
Wire Wire Line
	5400 4250 5450 4250
Text GLabel 5450 4250 2    50   Input ~ 0
row11
Text GLabel 6900 1100 0    50   Input ~ 0
row11
Wire Wire Line
	6900 800  6950 800 
Wire Wire Line
	6950 900  6900 900 
Wire Wire Line
	6950 1000 6900 1000
Wire Wire Line
	6950 1100 6900 1100
Text GLabel 6900 1000 0    50   Input ~ 0
row10
Wire Wire Line
	6750 3750 7050 3750
Wire Wire Line
	7050 3650 6750 3650
Wire Wire Line
	6750 3550 7050 3550
Wire Wire Line
	7050 3450 6750 3450
Wire Wire Line
	6750 3350 7050 3350
Wire Wire Line
	7050 3250 6750 3250
Wire Wire Line
	6750 3150 7050 3150
Wire Wire Line
	6750 3050 7050 3050
$Comp
L Connector_Generic:Conn_01x12 J3
U 1 1 5F673091
P 7250 3550
F 0 "J3" H 7330 3542 50  0000 L CNN
F 1 "Rows" H 7330 3451 50  0000 L CNN
F 2 "common:triomate-rows12" H 7250 3550 50  0001 C CNN
F 3 "~" H 7250 3550 50  0001 C CNN
	1    7250 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4650 4400 4650
Connection ~ 4400 4650
Wire Wire Line
	4400 4650 4500 4650
Connection ~ 4500 4650
Wire Wire Line
	4500 4650 4600 4650
Connection ~ 4600 4650
Wire Wire Line
	4600 4650 4700 4650
$Comp
L Device:Jumper_NC_Dual JP2
U 1 1 61619C80
P 3150 5950
F 0 "JP2" V 3196 6051 50  0000 L CNN
F 1 "Caps" V 3105 6051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3150 5950 50  0001 C CNN
F 3 "~" H 3150 5950 50  0001 C CNN
	1    3150 5950
	0    -1   -1   0   
$EndComp
$Comp
L Device:Jumper_NC_Dual JP3
U 1 1 61630AFF
P 3900 5950
F 0 "JP3" V 3946 6051 50  0000 L CNN
F 1 "Scroll" V 3855 6051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3900 5950 50  0001 C CNN
F 3 "~" H 3900 5950 50  0001 C CNN
	1    3900 5950
	0    -1   -1   0   
$EndComp
Connection ~ 1650 4850
Wire Wire Line
	7050 4950 7500 4950
Wire Wire Line
	7050 4750 7500 4750
Wire Wire Line
	7050 4650 7500 4650
Text GLabel 2350 6200 3    50   Input ~ 0
led2
$Comp
L Device:Jumper_NC_Dual JP1
U 1 1 61600B1B
P 2350 5950
F 0 "JP1" V 2396 6052 50  0000 L CNN
F 1 "Num" V 2305 6052 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2350 5950 50  0001 C CNN
F 3 "~" H 2350 5950 50  0001 C CNN
	1    2350 5950
	0    -1   -1   0   
$EndComp
Text GLabel 3150 5700 1    50   Input ~ 0
row8
Text GLabel 2350 5700 1    50   Input ~ 0
row9
Text GLabel 3150 6200 3    50   Input ~ 0
led1
Text GLabel 3900 6200 3    50   Input ~ 0
led0
Text GLabel 3900 5700 1    50   Input ~ 0
row10
$Comp
L Device:Jumper_NC_Dual JP4
U 1 1 6179E494
P 4750 5950
F 0 "JP4" V 4796 6051 50  0000 L CNN
F 1 "Power" V 4705 6051 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4750 5950 50  0001 C CNN
F 3 "~" H 4750 5950 50  0001 C CNN
	1    4750 5950
	0    -1   -1   0   
$EndComp
Text GLabel 5350 5600 2    50   Input ~ 0
row11
Wire Wire Line
	6850 4150 7050 4150
Wire Wire Line
	6850 4050 7050 4050
Wire Wire Line
	6850 3850 7050 3850
$Comp
L Connector_Generic:Conn_01x20 J2
U 1 1 5E88F7EA
P 7150 1700
F 0 "J2" H 7230 1692 50  0000 L CNN
F 1 "Cols" H 7230 1601 50  0000 L CNN
F 2 "common:triomate-cols20" H 7150 1700 50  0001 C CNN
F 3 "~" H 7150 1700 50  0001 C CNN
	1    7150 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 618278F8
P 4750 6200
F 0 "#PWR0101" H 4750 6050 50  0001 C CNN
F 1 "+3.3V" H 4765 6373 50  0000 C CNN
F 2 "" H 4750 6200 50  0001 C CNN
F 3 "" H 4750 6200 50  0001 C CNN
	1    4750 6200
	-1   0    0    1   
$EndComp
Text GLabel 4000 5950 2    50   Input ~ 0
select0
Text GLabel 4850 5950 2    50   Input ~ 0
select1
Text GLabel 6550 3950 0    50   Input ~ 0
select1
Text GLabel 6550 3850 0    50   Input ~ 0
select0
Text GLabel 6550 4050 0    50   Input ~ 0
select2
Text GLabel 6550 4150 0    50   Input ~ 0
select3
Text GLabel 7000 5600 0    50   Input ~ 0
select0
Text GLabel 7100 5950 0    50   Input ~ 0
select2
Text GLabel 7050 6150 0    50   Input ~ 0
select3
Text GLabel 3250 5950 2    50   Input ~ 0
select2
Text GLabel 2450 5950 2    50   Input ~ 0
select3
Wire Wire Line
	2650 4000 2550 4000
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 6189298E
P 7700 4750
F 0 "J5" H 7780 4742 50  0000 L CNN
F 1 "122LEDS" H 7780 4651 50  0000 L CNN
F 2 "common:triomate-leds4" H 7700 4750 50  0001 C CNN
F 3 "~" H 7700 4750 50  0001 C CNN
	1    7700 4750
	1    0    0    -1  
$EndComp
Connection ~ 7500 4650
Wire Wire Line
	7500 4650 8100 4650
Connection ~ 7500 4750
Wire Wire Line
	7500 4750 8100 4750
Wire Wire Line
	7500 4850 8100 4850
Connection ~ 7500 4950
Wire Wire Line
	7500 4950 8100 4950
Wire Wire Line
	1150 1100 1800 1100
Wire Wire Line
	1150 1300 1800 1300
Wire Wire Line
	1150 1400 1800 1400
$Comp
L power:GND #PWR0102
U 1 1 618EDA0C
P 1600 3300
F 0 "#PWR0102" H 1600 3050 50  0001 C CNN
F 1 "GND" H 1605 3127 50  0000 C CNN
F 2 "" H 1600 3300 50  0001 C CNN
F 3 "" H 1600 3300 50  0001 C CNN
	1    1600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3300 1600 3250
Wire Wire Line
	1050 3050 1050 3150
Wire Wire Line
	950  3050 1050 3050
Connection ~ 1050 3050
Wire Wire Line
	850  1800 1250 1800
Wire Wire Line
	1050 3150 1550 3150
$Comp
L power:+5V #PWR0105
U 1 1 61910EFB
P 1700 2250
F 0 "#PWR0105" H 1700 2100 50  0001 C CNN
F 1 "+5V" H 1715 2423 50  0000 C CNN
F 2 "" H 1700 2250 50  0001 C CNN
F 3 "" H 1700 2250 50  0001 C CNN
	1    1700 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6191130C
P 1500 2650
F 0 "R2" V 1600 2650 50  0000 L CNN
F 1 "22" V 1600 2500 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1430 2650 50  0001 C CNN
F 3 "~" H 1500 2650 50  0001 C CNN
F 4 "C23345" H 1500 2650 50  0001 C CNN "LCSC"
	1    1500 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 61911317
P 1500 2750
F 0 "R5" V 1400 2750 50  0000 L CNN
F 1 "22" V 1400 2600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.20x1.40mm_HandSolder" V 1430 2750 50  0001 C CNN
F 3 "~" H 1500 2750 50  0001 C CNN
F 4 "C23345" H 1500 2750 50  0001 C CNN "LCSC"
	1    1500 2750
	0    -1   -1   0   
$EndComp
Text GLabel 2100 2650 2    50   Input ~ 0
D+
Text GLabel 2100 2750 2    50   Input ~ 0
D-
Wire Wire Line
	1650 2450 1700 2450
Wire Wire Line
	1700 2450 1700 2250
Wire Wire Line
	2100 2750 1650 2750
$Comp
L Device:R R1
U 1 1 6191132B
P 1500 2450
F 0 "R1" V 1293 2450 50  0000 C CNN
F 1 "0" V 1384 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.30x2.65mm_HandSolder" V 1430 2450 50  0001 C CNN
F 3 "~" H 1500 2450 50  0001 C CNN
F 4 "C17477" H 1500 2450 50  0001 C CNN "LCSC"
	1    1500 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	1650 2650 2100 2650
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 61958056
P 1400 3150
F 0 "J8" H 1508 3331 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1508 3240 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-1sqmm_1x02_P5.4mm_D1.4mm_OD2.7mm" H 1400 3150 50  0001 C CNN
F 3 "~" H 1400 3150 50  0001 C CNN
F 4 "Mouser" H 1400 3150 50  0001 C CNN "Distrib 1"
F 5 "571-928814-1" H 1400 3150 50  0001 C CNN "Distrib 1 PN"
	1    1400 3150
	1    0    0    -1  
$EndComp
Connection ~ 1600 3250
Wire Wire Line
	1600 3250 1550 3150
Connection ~ 1550 3150
Wire Wire Line
	1550 3150 1600 3150
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 619726BB
P 1050 1800
F 0 "J7" H 1158 1981 50  0000 C CNN
F 1 "J7" H 1158 1890 50  0000 C CNN
F 2 "Connector_Wire:SolderWire-1sqmm_1x02_P5.4mm_D1.4mm_OD2.7mm" H 1050 1800 50  0001 C CNN
F 3 "~" H 1050 1800 50  0001 C CNN
F 4 "Mouser" H 1050 1800 50  0001 C CNN "Distrib 1"
F 5 "571-928814-1" H 1050 1800 50  0001 C CNN "Distrib 1 PN"
	1    1050 1800
	1    0    0    -1  
$EndComp
Connection ~ 1250 1800
Wire Wire Line
	1250 1800 1400 1800
Wire Wire Line
	1250 1800 1250 1900
$Comp
L power:+5V #PWR0106
U 1 1 61992A87
P 4750 5700
F 0 "#PWR0106" H 4750 5550 50  0001 C CNN
F 1 "+5V" H 4765 5873 50  0000 C CNN
F 2 "" H 4750 5700 50  0001 C CNN
F 3 "" H 4750 5700 50  0001 C CNN
	1    4750 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 619B15A2
P 5150 5500
F 0 "J9" H 5258 5681 50  0000 C CNN
F 1 "SWDIO to Row11" H 5258 5590 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5150 5500 50  0001 C CNN
F 3 "~" H 5150 5500 50  0001 C CNN
	1    5150 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5500 5350 5600
$Comp
L Device:R R10
U 1 1 619CFD91
P 6700 3850
F 0 "R10" V 6650 4000 50  0000 C CNN
F 1 "100" V 6700 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6630 3850 50  0001 C CNN
F 3 "~" H 6700 3850 50  0001 C CNN
F 4 "C17408" H 6700 3850 50  0001 C CNN "LCSC"
	1    6700 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 619D7759
P 6700 3950
F 0 "R11" V 6650 4100 50  0000 C CNN
F 1 "100" V 6700 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6630 3950 50  0001 C CNN
F 3 "~" H 6700 3950 50  0001 C CNN
F 4 "C17408" H 6700 3950 50  0001 C CNN "LCSC"
	1    6700 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 619D8111
P 6700 4050
F 0 "R12" V 6650 4200 50  0000 C CNN
F 1 "100" V 6700 4050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6630 4050 50  0001 C CNN
F 3 "~" H 6700 4050 50  0001 C CNN
F 4 "C17408" H 6700 4050 50  0001 C CNN "LCSC"
	1    6700 4050
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 619D944D
P 6700 4150
F 0 "R13" V 6650 4300 50  0000 C CNN
F 1 "100" V 6700 4150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6630 4150 50  0001 C CNN
F 3 "~" H 6700 4150 50  0001 C CNN
F 4 "C17408" H 6700 4150 50  0001 C CNN "LCSC"
	1    6700 4150
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 619DD861
P 2600 5050
F 0 "TP2" H 2542 5076 50  0000 R CNN
F 1 "TestPoint" H 2542 5167 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 2800 5050 50  0001 C CNN
F 3 "~" H 2800 5050 50  0001 C CNN
	1    2600 5050
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 619DDD41
P 2600 5050
F 0 "#PWR0107" H 2600 4900 50  0001 C CNN
F 1 "+5V" H 2615 5223 50  0000 C CNN
F 2 "" H 2600 5050 50  0001 C CNN
F 3 "" H 2600 5050 50  0001 C CNN
	1    2600 5050
	1    0    0    -1  
$EndComp
Text GLabel 6450 4850 0    50   Input ~ 0
select1
Wire Wire Line
	6450 4850 7500 4850
Connection ~ 7500 4850
Text GLabel 6450 4950 0    50   Input ~ 0
led0
Text GLabel 6450 4750 0    50   Input ~ 0
led1
Text GLabel 6450 4650 0    50   Input ~ 0
led2
$EndSCHEMATC
