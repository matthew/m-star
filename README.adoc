= M-Star upgrades for buckling-string keyboards

Drop-in replacement controller boards for vintage and modern IBM Model
Ms.

Vintage Model Ms are wonderful keyboards but they ship archaic wire
protols like PC/AT and PS/2. When plugged into USB adapters there are
often chronic issues due to the controllers wanting to draw more power
than modern USB hubs are really equipped to supply.

Modern Model Ms from Unicomp are regrettaby prone to firmwarw bugs.

The only complete solution to these problems is to replace the
controller electronics with a PCB that ships USB natively.

This project hosts an open-source hardware designs to do that.
Because previous attempts at similar projects have gotten to a working
prototype and then stalled out before getting to volume production, we
will emphasize making decisions that support low-cost volume
production.

The project has a https://esr.gitlab.io/m-star[public website].
You can find a a more detailed rationale and design discussion there.
